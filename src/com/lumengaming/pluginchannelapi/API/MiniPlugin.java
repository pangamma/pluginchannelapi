package com.lumengaming.pluginchannelapi.API;

import com.lumengaming.pluginchannelapi.API.incoming.ServerType;
import com.lumengaming.pluginchannelapi.API.outgoing.OutgoingChannel;

public interface MiniPlugin {

    /**
     * Returns true if the specified plugin is run with BungeeCord.
     * @return
     */
    public boolean isBungeecord();

    /**
     * Returns the plugin instance;
     * @return
     */
    public Object getPlugin();

    /**
     * Returns the current BungeeCord server.
     * @return
     */
    public String getServerName();
    
    public ServerType getSourceType();
    
    public OutgoingChannel getOutgoingChannel();
}
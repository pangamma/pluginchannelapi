package com.lumengaming.pluginchannelapi.API.outgoing;

import com.lumengaming.pluginchannelapi.API.MessageJson;
import com.lumengaming.pluginchannelapi.API.PluginChannelMessageEventBungee;
import com.lumengaming.pluginchannelapi.STATIC;
import com.lumengaming.pluginchannelapi.bungee.BungeeMain;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.plugin.Plugin;
import org.json.simple.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Redraskal_2 on 11/10/2016.
 */
public class OutgoingProxyChannel implements OutgoingChannel {

    private final BungeeMain plugin;

    /**
     * Registers an outgoing channel.
     *
     * @param instance
     */
    public OutgoingProxyChannel(BungeeMain plugin) {
        this.plugin = plugin;
    }

    public boolean send(JSONObject encodedJson, String server) {
        if (!this.plugin.isBungeecord() || server.equalsIgnoreCase("BungeeCord")) {
            return false;
        }
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        DataOutputStream out = new DataOutputStream(stream);
        ServerInfo serverInfo = ((Plugin) plugin).getProxy().getServerInfo(server);
        //JSONObject encodedJson = Formatter.formatMessage(this.plugin.getChannel(), (String) data, server, "BungeeCord", Source.PROXY);
        ((Plugin) plugin).getLogger().info("Proxy ==> Spigot: " + encodedJson.toJSONString());
        try {
            out.writeUTF(encodedJson.toJSONString());
            serverInfo.sendData(STATIC.PLUGIN_CHANNEL_NAME, stream.toByteArray());
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public boolean sendToProxy(MessageJson encodedJson) {
        if (!this.plugin.isBungeecord()) {
            return false;
        }
        PluginChannelMessageEventBungee event = new PluginChannelMessageEventBungee(
                encodedJson.getChannel(),
                encodedJson.getMessage(),
                encodedJson.getSourceName(),
                encodedJson.getSourceType()
        );
        ((Plugin) plugin).getProxy().getPluginManager().callEvent(event);
        ((Plugin) plugin).getLogger().info("Proxy ==> Spigot: " + encodedJson.toJSONString());
        return true;
    }

    @Override
    public boolean sendToAllServers(MessageJson encodedJson) {
        try {
            for (ServerInfo allServers : ((Plugin) plugin).getProxy().getServers().values()) {
                sendData(encodedJson.toJSONString(), allServers);
            }
            return true;
        } catch (IOException ex) {
            Logger.getLogger(OutgoingProxyChannel.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    /**
     * The fancy backend method of sending data over to Spigot servers.
     *
     * @param channel
     * @param message
     * @param serverInfo
     * @throws IOException
     */
    public boolean sendData(String message, ServerInfo serverInfo) throws IOException {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        DataOutputStream out = new DataOutputStream(stream);
        try{
            out.writeUTF(message);
            serverInfo.sendData(STATIC.PLUGIN_CHANNEL_NAME, stream.toByteArray());
            return true;
        }catch(Exception ex){
            return false;
        }
    }
}

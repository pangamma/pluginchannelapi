package com.lumengaming.pluginchannelapi.API.outgoing;
import com.lumengaming.pluginchannelapi.API.MessageJson;
import com.lumengaming.pluginchannelapi.API.MiniPlugin;
import com.lumengaming.pluginchannelapi.API.incoming.ServerType;
import com.lumengaming.pluginchannelapi.STATIC;
import com.lumengaming.pluginchannelapi.spigot.SpigotMain;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Created by Redraskal_2 on 11/10/2016.
 */
public class OutgoingSpigotChannel implements OutgoingChannel {
    private final MiniPlugin plugin;


    /**
     * Registers an outgoing channel.
     * @param instance
     */
    public OutgoingSpigotChannel(SpigotMain plugin) {
        this.plugin = plugin;
    }
    
    /**
     * Returns a random player online,
     * null if no one is available.
     * @return
     */
    private Player randomPlayer() {
        if(Bukkit.getOnlinePlayers().size() <= 0)
            return null;
        Player[] players = Bukkit.getOnlinePlayers().toArray(new Player[Bukkit.getOnlinePlayers().size()]);
        return players[ThreadLocalRandom.current().nextInt(players.length)];
    }

    @Override
    public boolean sendToProxy(MessageJson encodedJson) {
        if(!this.plugin.isBungeecord() || this.randomPlayer() == null || encodedJson.getDestinationType() != ServerType.PROXY)
            return false;
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        DataOutputStream out = new DataOutputStream(stream);
        ((JavaPlugin)this.plugin).getLogger().info("Spigot ==> Proxy: " + encodedJson.toJSONString());
        try {
            out.writeUTF(encodedJson.toJSONString());
            this.randomPlayer().sendPluginMessage((org.bukkit.plugin.Plugin) plugin, STATIC.PLUGIN_CHANNEL_NAME, stream.toByteArray());
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public boolean sendToAllServers(MessageJson encodedJson) {
        if(this.plugin.isBungeecord() || this.randomPlayer() == null || encodedJson.getDestinationType() != ServerType.SPIGOT)
            return false;
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        DataOutputStream out = new DataOutputStream(stream);
        ((JavaPlugin)this.plugin).getLogger().info("Spigot ==> Proxy: " + encodedJson.toJSONString());
        try {
            out.writeUTF(encodedJson.toJSONString());
            this.randomPlayer().sendPluginMessage((org.bukkit.plugin.Plugin) plugin, STATIC.PLUGIN_CHANNEL_NAME, stream.toByteArray());
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}

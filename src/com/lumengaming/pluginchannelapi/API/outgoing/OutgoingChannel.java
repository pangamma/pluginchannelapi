package com.lumengaming.pluginchannelapi.API.outgoing;

import com.lumengaming.pluginchannelapi.API.MessageJson;

public interface OutgoingChannel {

    /**
     * Sends data to the specified server. (Use BungeeCord for the proxy)
     * @param data
     * @param server
     * @return
     */
    public boolean sendToProxy(MessageJson encodedJson);
    /**
     * Sends data to the specified server. (Use BungeeCord for the proxy)
     * @param data
     * @param server
     * @return
     */
    public boolean sendToAllServers(MessageJson encodedJson);
}
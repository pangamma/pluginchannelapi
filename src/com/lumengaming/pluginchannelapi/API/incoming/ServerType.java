package com.lumengaming.pluginchannelapi.API.incoming;

/**
 * Created by Redraskal_2 on 11/10/2016.
 */
public enum ServerType {

    PROXY(0),
    SPIGOT(1);

    private int id;

    private ServerType(int id) {
        this.id = id;
    }

    public int getID() {
        return this.id;
    }

    public static ServerType fromID(int id) {
        for(ServerType source : ServerType.values()) {
            if(source.getID() == id) {
                return source;
            }
        }
        return null;
    }
}
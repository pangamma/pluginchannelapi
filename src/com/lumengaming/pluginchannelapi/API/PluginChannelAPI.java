package com.lumengaming.pluginchannelapi.API;

import com.lumengaming.pluginchannelapi.API.incoming.ServerType;

/**
 *
 * @author Taylor
 */
public class PluginChannelAPI {

    
    private MiniPlugin plugin = null;
    private static PluginChannelAPI self = null;
    
    public static PluginChannelAPI _init(MiniPlugin plugin){
        PluginChannelAPI.self = new PluginChannelAPI(plugin);
        return self;
    }
    public static void _destroy() {
        self = null;
    }
    
    private PluginChannelAPI(MiniPlugin aThis) {
        this.plugin = aThis;
    }
    
    /**
     * Returns NULL if not available.
     * @return 
     */
    public static PluginChannelAPI getInstance(){
        return self;
    }
    
    public boolean sendToAllServers(String channel, String message){
        MessageJson json = new MessageJson(channel, message, plugin, ServerType.SPIGOT);
        this.plugin.getOutgoingChannel().sendToAllServers(json);
        return false;
    }
    
    public boolean sendToProxy(String channel, String message){
        MessageJson json = new MessageJson(channel, message, plugin, ServerType.PROXY);
        this.plugin.getOutgoingChannel().sendToProxy(json);
        return false;
    }     
}

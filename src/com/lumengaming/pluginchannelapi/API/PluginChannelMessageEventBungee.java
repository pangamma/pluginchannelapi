package com.lumengaming.pluginchannelapi.API;
import com.lumengaming.pluginchannelapi.API.incoming.ServerType;
import net.md_5.bungee.api.plugin.Cancellable;
import net.md_5.bungee.api.plugin.Event;


public class PluginChannelMessageEventBungee extends Event implements Cancellable{
    private boolean isCancelled = false;
    private String channel;
    private String message;
    private String fromServerName;
    private ServerType fromServerType;

    public PluginChannelMessageEventBungee(String channel, String message, String fromServerName, ServerType fromServerType) {
        this.channel = channel;
        this.message = message;
        this.fromServerName = fromServerName;
        this.fromServerType = fromServerType;
    }
    
    @Override
    public boolean isCancelled() {
        return this.isCancelled;
    }

    @Override
    public void setCancelled(boolean bln) {
        this.isCancelled = bln;
    }

    public String getChannel() {
        return channel;
    }

    public String getMessage() {
        return message;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getSourceName() {
        return fromServerName;
    }

    public ServerType getSourceType() {
        return fromServerType;
    }
    
    @Override
    public void postCall(){
        super.postCall();
    }
    
}

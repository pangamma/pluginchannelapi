package com.lumengaming.pluginchannelapi.API;
import com.lumengaming.pluginchannelapi.API.incoming.ServerType;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;


public class PluginChannelMessageEventSpigot extends Event implements Cancellable{
	private final static HandlerList handlers  = new HandlerList();
    private boolean isCancelled = false;
    private String channel;
    private String message;
    private String sourceName;
    private ServerType sourceType;

    public PluginChannelMessageEventSpigot(String channel, String message, String sourceName, ServerType sourceType) {
        super(false); // async = b{input}
        this.channel = channel;
        this.message = message;
        this.sourceName = sourceName;
        this.sourceType = sourceType;
    }
    
	@Override
	public HandlerList getHandlers() {
		return PluginChannelMessageEventSpigot.handlers;
	}

    @Override
    public boolean isCancelled() {
        return this.isCancelled;
    }

    @Override
    public void setCancelled(boolean bln) {
        this.isCancelled = bln;
    }

    public String getChannel() {
        return channel;
    }

    public String getMessage() {
        return message;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getSourceName() {
        return sourceName;
    }

    public ServerType getSourceType() {
        return sourceType;
    }
    
}

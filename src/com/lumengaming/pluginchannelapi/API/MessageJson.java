package com.lumengaming.pluginchannelapi.API;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.lumengaming.pluginchannelapi.API.incoming.ServerType;
import static com.lumengaming.pluginchannelapi.STATIC.*;
import org.bukkit.Bukkit;

/**
 *
 * @author Taylor
 */
public class MessageJson {
    private String channel;
    private String message;
    private String sourceName;
    private ServerType sourceType;
    private ServerType destinationType;
    
    public MessageJson(String channel, String message, MiniPlugin plugin, ServerType destinationType) {
        this.channel = channel;
        this.message = message;
        this.sourceName = plugin.getServerName();
        this.sourceType = plugin.getSourceType();
        this.destinationType = destinationType;
    }
    private MessageJson(){}
    
    public JsonObject toJson(){
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty(CHANNEL, channel);
        jsonObject.addProperty(MESSAGE, message);
        jsonObject.addProperty(FROM_SERVER_NAME, sourceName);
        jsonObject.addProperty(SOURCE_TYPE, sourceType.getID());
        jsonObject.addProperty(DESTINATION_TYPE, destinationType.getID());
        return jsonObject;
    }
    
    public static MessageJson fromJson(String jsonStr){
        try{
            JsonObject obj = new JsonParser().parse(jsonStr).getAsJsonObject();
            return fromJson(obj);
        }catch(Exception ex){ex.printStackTrace();}
        return null;
    }
    
    public static MessageJson fromJson(JsonObject obj){
        try{
            MessageJson msg = new MessageJson();
            msg.channel = obj.get(CHANNEL).getAsString();
            msg.message = obj.get(MESSAGE).getAsString();
            msg.sourceType = ServerType.fromID(obj.get(SOURCE_TYPE).getAsInt());
            msg.sourceName = obj.get(FROM_SERVER_NAME).getAsString();
            msg.destinationType = ServerType.fromID(obj.get(DESTINATION_TYPE).getAsInt());
            return msg;
        }catch(Exception ex){ex.printStackTrace();}
        return null;
    }

    public String getChannel() {
        return channel;
    }

    public String getMessage() {
        return message;
    }

    public String getSourceName() {
        return sourceName;
    }

    public ServerType getSourceType() {
        return sourceType;
    }

    public ServerType getDestinationType() {
        return destinationType;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setSourceName(String sourceName) {
        this.sourceName = sourceName;
    }

    public void setSourceType(ServerType sourceType) {
        this.sourceType = sourceType;
    }

    public void setDestinationType(ServerType destinationType) {
        this.destinationType = destinationType;
    }
    
    public String toJSONString(){
        String json = this.toJson().toString();
        return json;
    }
}

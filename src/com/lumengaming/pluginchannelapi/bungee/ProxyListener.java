package com.lumengaming.pluginchannelapi.bungee;

import com.lumengaming.pluginchannelapi.API.MessageJson;
import com.lumengaming.pluginchannelapi.API.MiniPlugin;
import com.lumengaming.pluginchannelapi.API.incoming.ServerType;
import com.lumengaming.pluginchannelapi.STATIC;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.event.PluginMessageEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.event.EventHandler;

import java.io.*;

public class ProxyListener implements Listener {

    private MiniPlugin instance;

    /**
     * Registers the listener for the specified plugin.
     * @param proxyPlugin
     */
    public ProxyListener(MiniPlugin proxyPlugin) {
        this.instance = proxyPlugin;
    }

    @EventHandler
    public void onPluginMessage(PluginMessageEvent pluginMessageEvent) {
        ((Plugin) instance).getProxy().getLogger().info("Spigot ==> Proxy: " + pluginMessageEvent.getTag() + ", " + pluginMessageEvent.getData());
        if(!pluginMessageEvent.getTag().equalsIgnoreCase(STATIC.PLUGIN_CHANNEL_NAME))
            return;
        DataInputStream in = new DataInputStream(new ByteArrayInputStream(pluginMessageEvent.getData()));
        try {
            String decode = in.readUTF();
            if(decode != null && !decode.isEmpty()) {
                // CHANNEL / INCOMING OBJECT / SOURCE
                MessageJson json = MessageJson.fromJson(decode);
                ((Plugin) instance).getProxy().getLogger().info("Spigot ==> Proxy: " + json.toJSONString());
                json.setSourceName(((Plugin) instance).getProxy().getInstance().getPlayer(pluginMessageEvent.getReceiver().toString()).getServer().getInfo().getName());
                

                if(json.getDestinationType() == ServerType.PROXY) {
                    instance.getOutgoingChannel().sendToProxy(json);
                } else {
                    // SPIGOT_PLUGIN => BungeeCord (relay node) => ANOTHER SPIGOT_PLUGIN
                    for(ServerInfo allServers : ((Plugin) instance).getProxy().getServers().values()) {
                        this.sendData(json.getChannel(), json.toJSONString(),allServers);
                    }
                }
            }
        } catch (Exception e) {}
    }

    /**
     * The fancy backend method of sending data over to Spigot servers.
     * @param channel
     * @param message
     * @param serverInfo
     * @throws IOException
     */
    public void sendData(String channel, String message, ServerInfo serverInfo) throws IOException {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        DataOutputStream out = new DataOutputStream(stream);
        out.writeUTF(message);
        serverInfo.sendData(STATIC.PLUGIN_CHANNEL_NAME, stream.toByteArray());
    }
}

package com.lumengaming.pluginchannelapi.bungee;

import com.lumengaming.pluginchannelapi.API.MiniPlugin;
import com.lumengaming.pluginchannelapi.API.PluginChannelAPI;
import com.lumengaming.pluginchannelapi.API.incoming.ServerType;
import com.lumengaming.pluginchannelapi.API.outgoing.OutgoingChannel;
import com.lumengaming.pluginchannelapi.API.outgoing.OutgoingProxyChannel;
import com.lumengaming.pluginchannelapi.STATIC;
import net.md_5.bungee.api.plugin.Plugin;

/**
 * Created by Redraskal_2 on 11/10/2016.
 */
public class BungeeMain extends Plugin implements MiniPlugin {

    private BungeeMain instance;
    private ProxyListener proxyListener;
    private OutgoingProxyChannel outgoingChannel;

    @Override
    public void onEnable() {
        instance = this;
        this.proxyListener = new ProxyListener(instance);
        this.getProxy().registerChannel(STATIC.PLUGIN_CHANNEL_NAME);
        this.getProxy().getPluginManager().registerListener(this, this.proxyListener);
        this.outgoingChannel = new OutgoingProxyChannel(this);
        PluginChannelAPI._init(this);
    
    }

    @Override
    public void onDisable() {
        this.getProxy().unregisterChannel(STATIC.PLUGIN_CHANNEL_NAME);
        this.getProxy().getPluginManager().unregisterListener(this.proxyListener);
        this.proxyListener = null;
        this.instance = null;
        PluginChannelAPI._destroy();
    }

    @Override
    public boolean isBungeecord() {
        return true;
    }

    @Override
    public Object getPlugin() {
        return this;
    }

    @Override
    public String getServerName() {
        return "BungeeCord";
    }

    @Override
    public OutgoingChannel getOutgoingChannel() {
        return this.outgoingChannel;
    }

    @Override
    public ServerType getSourceType() {
        return ServerType.PROXY;
    }
}
package com.lumengaming.pluginchannelapi;

import com.lumengaming.pluginchannelapi.API.MiniPlugin;
import com.lumengaming.pluginchannelapi.API.incoming.ServerType;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 *
 * @author Taylor
 */
public class STATIC {
    
    public static final String CHANNEL = "c";
    public static final String MESSAGE = "m";
    public static final String FROM_SERVER_NAME = "fs";
    public static final String SOURCE_TYPE = "st";
    public static final String DESTINATION_TYPE = "dt";
    public static final String PLUGIN_CHANNEL_NAME = "PluginChannelAPI";
    
    /**
     * Formats a message to be sent.
     * @param channel
     * @param message
     * @param plugin
     * @param destinationType
     * @return
     */
    public static JSONObject formatMessage(String channel, String message, MiniPlugin plugin, ServerType destinationType) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put(CHANNEL, channel);
        jsonObject.put(MESSAGE, message);
        jsonObject.put(FROM_SERVER_NAME, plugin.getServerName());
        jsonObject.put(SOURCE_TYPE, plugin.getSourceType().getID());
        jsonObject.put(DESTINATION_TYPE, destinationType.getID());
        return jsonObject;
    }  
    
    /**
     * Parses a message received by the server.
     * @param message
     * @return
     * @throws ParseException
     */
    public static JSONObject parseMessage(String message) throws ParseException {
        return (JSONObject) new JSONParser().parse(message);
    }
}

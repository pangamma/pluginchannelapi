package com.lumengaming.pluginchannelapi.spigot;

import com.lumengaming.pluginchannelapi.API.MessageJson;
import com.lumengaming.pluginchannelapi.API.PluginChannelMessageEventSpigot;
import com.lumengaming.pluginchannelapi.STATIC;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.plugin.messaging.PluginMessageListener;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;

/**
 * Created by Redraskal_2 on 11/10/2016.
 */
public class SpigotListener implements PluginMessageListener {

    private SpigotMain instance;

    /**
     * Registers the listener for the specified plugin.
     * @param spigotPlugin
     */
    public SpigotListener(SpigotMain spigotPlugin) {
        this.instance = spigotPlugin;
    }

    @Override
    public void onPluginMessageReceived(String channel, Player player, byte[] message) {
        ((JavaPlugin) instance).getLogger().info("Proxy ==> Spigot: " + channel + ", " + message);
        if(!channel.equalsIgnoreCase(STATIC.PLUGIN_CHANNEL_NAME))
            return;
        DataInputStream in = new DataInputStream(new ByteArrayInputStream(message));
        try {
            String decode = in.readUTF();
            if(decode != null && !decode.isEmpty()) {
                // CHANNEL / INCOMING OBJECT / SOURCE
                MessageJson json = MessageJson.fromJson(decode);
                ((JavaPlugin) instance).getLogger().info("Proxy ==> Spigot: " + json.toJSONString());
                PluginChannelMessageEventSpigot event = new PluginChannelMessageEventSpigot(json.getChannel(),json.getMessage(),json.getSourceName(),json.getSourceType());
                Bukkit.getPluginManager().callEvent(event);
            }
        } catch (Exception e) {}
    }
}

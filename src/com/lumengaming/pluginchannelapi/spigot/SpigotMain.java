package com.lumengaming.pluginchannelapi.spigot;

import com.lumengaming.pluginchannelapi.API.MiniPlugin;
import com.lumengaming.pluginchannelapi.API.PluginChannelAPI;
import com.lumengaming.pluginchannelapi.API.incoming.ServerType;
import com.lumengaming.pluginchannelapi.API.outgoing.OutgoingChannel;
import com.lumengaming.pluginchannelapi.API.outgoing.OutgoingSpigotChannel;
import com.lumengaming.pluginchannelapi.STATIC;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

/**
 *
 * @author Taylor
 */
public class SpigotMain extends JavaPlugin implements MiniPlugin{

    private SpigotMain instance;
    private SpigotListener spigotListener;
    private OutgoingSpigotChannel outgoingChannel;

    @Override
    public void onEnable() {
        instance = this;
        this.spigotListener = new SpigotListener(this);
        Bukkit.getMessenger().registerOutgoingPluginChannel((org.bukkit.plugin.Plugin) this, STATIC.PLUGIN_CHANNEL_NAME);
        Bukkit.getMessenger().registerIncomingPluginChannel((org.bukkit.plugin.Plugin) this, STATIC.PLUGIN_CHANNEL_NAME, this.spigotListener);
        this.outgoingChannel = new OutgoingSpigotChannel(this);
        PluginChannelAPI._init(this);
    }

    @Override
    public void onDisable() {
        Bukkit.getMessenger().unregisterIncomingPluginChannel(this, STATIC.PLUGIN_CHANNEL_NAME, spigotListener);
        Bukkit.getMessenger().unregisterOutgoingPluginChannel(this,STATIC.PLUGIN_CHANNEL_NAME);
        this.spigotListener = null; // helps with garbage collection.
        this.instance = null;
        PluginChannelAPI._destroy();
    }
    
    @Override
    public boolean isBungeecord() {
        return false;
    }

    @Override
    public Object getPlugin() {
        return this;
    }

    @Override
    public String getServerName() {
        return Bukkit.getIp();
    }

    @Override
    public OutgoingChannel getOutgoingChannel() {
        return this.outgoingChannel;
    }

    @Override
    public ServerType getSourceType() {
        return ServerType.SPIGOT;
    }
}
